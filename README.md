# standAloneTTrackRecoU1b

Code for the Upgrade Ib Mighty Tracker design optimization study using stand-alone T-Track reconstruction, the summer student project of Chengxi Yang supervised by Lucia Grillo, Oscar Augusto De Aguiar Francisco, and Christopher Parkes.

My email: chengxi_yang@mail.ustc.edu.cn

Summer Student Report: https://cds.cern.ch/record/2783224

## Folder A:
The first stage of the study, using equivalent layers and effective resolutions.
### blurHits.cpp
Blur the MCTruth hits to produce the spatial resolution of SciFi and silicon, generate a root file for the pattern recognition algorithm.
Please:
1. Change the Input MCNTuple File Directory, set the Output File Directory
2. Change the "surface" to define coverage
3. If blurIt == true, blur the hits with effective resolutions
4. Change the line below: "CHANGE HERE TO ADD MORE SILICON" to add more silicon layers.
5. root -p blurHits.cpp
### patternReco.cpp
1. Change the Input File Directory (NOT THE MCNTuple File, use the file generated with blurHits.cpp)
2. root -p patternReco.cpp

## Folder B:
The second stage of the study, using x-u-v-x layers for SciFi and equivalent Hybrid layers.


### Folder 2Layers
The scripts for 2-Hybrid-Layer designs
#### samplePrep.cpp
1. Change the Input MCNTuple File Directory, set the Output File Directory
2. Change the "surface" to define coverage
3. If blurIt == true, blur the hits with effective resolutions
4. root -p blurHits.cpp

#### stereoSeeding.cpp
1. Change the Input File Directory (NOT THE MCNTuple File, use the file generated with samplePrep.cpp IN THE SAME NLayers folder)
2. root -p patternReco.cpp

### Folder 3Layers
The scripts for 3-Hybrid-Layer designs.
Usage is the same as 2Layers.

